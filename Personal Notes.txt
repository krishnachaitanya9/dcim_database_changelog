First create database:
	create database dcim_copy;
Once it's create you can copy all contents from dcim database to to dcim_copy using mysqldump command:
	mysqldump -u root -prootpw dcim | mysql -u root -prootpw dcim_copy
Now from time to time there is a need to to delete all tables for copying new tables data. For this execute this command:
	mysqldump -u root --add-drop-table --no-data -p dcim_copy -prootpw | grep 'DROP TABLE' > drop_all_tables.sql
	mysql -u root -prootpw dcim_copy < drop_all_tables.sql
To delete all contents from tables in a database:
	mysqldump -u root --add-drop-table --no-data -p dcim_copy -prootpw | grep 'DROP TABLE' > drop_all_tables.sql
	sed -i 's/DROP TABLE IF EXISTS/DELETE FROM/g' drop_all_tables.sql
	mysql -u root -prootpw dcim_copy < drop_all_tables.sql

