

Addition = """SELECT {fieldnames}
FROM dcim.{tablename}
WHERE {incrementid} NOT IN
      (SELECT {incrementid}
        FROM dcimaudit.{tablename}
        )"""

Deletion = """SELECT {fieldnames}
FROM dcimaudit.{tablename}
WHERE {incrementid} NOT IN
      (SELECT {incrementid}
        FROM dcim.{tablename}
        )"""