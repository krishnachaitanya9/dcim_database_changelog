SELECT DataCenterID,Name,Administrator
FROM dcim.fac_DataCenter
WHERE DataCenterID NOT IN
      (SELECT DataCenterID
        FROM dcim_copy.fac_DataCenter
        );

SELECT DataCenterID,Name,Administrator
FROM dcim_copy.fac_DataCenter
WHERE DataCenterID NOT IN
      (SELECT DataCenterID
        FROM dcim.fac_DataCenter
        )

