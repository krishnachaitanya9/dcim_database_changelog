import Config
from subprocess import DEVNULL, STDOUT, check_call

def copy_contents():
    mysql_command = 'mysqldump -u {originaldbuser} -p{originaldbpass} {originaldb} {tables_names}| /usr/local/mysql/bin/mysql -u {copydbuser} -p{copydbpass} {copydb}'.format(

        originaldbuser = Config.original_db_username,
        originaldbpass = Config.original_db_password,
        originaldb = Config.original_db_name,

        tables_names = Config.tables_names,

        copydbuser = Config.copy_db_username,
        copydbpass = Config.copy_db_password,
        copydb = Config.copy_db_name)

    check_call([mysql_command], stdout=DEVNULL, stderr=STDOUT, shell=True)

if __name__ == "__main__":
    copy_contents()