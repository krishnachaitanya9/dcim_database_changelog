import pymysql
import Config
def create_database():
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password)
    cursor = db.cursor()
    cursor.execute("CREATE DATABASE "+Config.copy_db_name)

if __name__ == "__main__":
    try:
        create_database()
    except pymysql.err.ProgrammingError:
        print('Database Exists')

