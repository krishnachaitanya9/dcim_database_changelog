import Config
from subprocess import DEVNULL, STDOUT, check_call


def delete_all_contents():
    mysql_command = 'mysqldump -u {copydbuser} --add-drop-table --no-data -p {copydb} -p{copydbpass} | grep \'DROP TABLE\' > {sqlfile}'.format(

        sqlfile = Config.sql_commands_file_name,
        copydbuser = Config.copy_db_username,
        copydbpass = Config.copy_db_password,
        copydb = Config.copy_db_name)
    check_call([mysql_command], stdout=DEVNULL, stderr=STDOUT, shell=True)
    mysql_command = 'sed -i \'s/DROP TABLE IF EXISTS/DELETE FROM/g\' {sqlfile}'.format(sqlfile = Config.sql_commands_file_name)
    check_call([mysql_command], stdout=DEVNULL, stderr=STDOUT, shell=True)
    mysql_command = '/usr/local/mysql/bin/mysql -u {copydbuser} -p{copydbpass} {copydb} < {sqlfile}'.format(

            sqlfile=Config.sql_commands_file_name,
            copydbuser=Config.copy_db_username,
            copydbpass=Config.copy_db_password,
            copydb=Config.copy_db_name)
    check_call([mysql_command], stdout=DEVNULL, stderr=STDOUT, shell=True)



if __name__ == "__main__":
    delete_all_contents()