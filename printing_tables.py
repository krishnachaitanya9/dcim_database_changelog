import pymysql
import Config
import Query_Strings
from htmltables import HTML_Page
from htmltables import HTML_table
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime
import os
import delete_contents_from_table
import copy_dcim_contents
import collections

def Query_field_generator(Query_String, fields_list, table_name, increment_id):
    return Query_String.format(fieldnames=','.join(fields_list), tablename=table_name, incrementid=increment_id)

def sendemail():
    emailfrom = Config.username
    emailto = Config.boss_email
    fileToSend = "test.html"
    html = open(fileToSend, 'r').read()
    username = Config.username
    password = Config.password

    msg = MIMEMultipart()
    msg["From"] = emailfrom
    msg["To"] = emailto
    msg["Cc"] = 'kodur.chaitanya@colorado.edu'
    msg["Subject"] = "OpenDCIM data Entries ChangeLog " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


    msg.attach(MIMEText(html, 'html'))


    server = smtplib.SMTP(Config.smtp_server, Config.smtp_port)
    server.starttls()
    server.login(username, password)
    server.sendmail(emailfrom, emailto, msg.as_string())
    server.quit()

def get_datacenter_name(id):
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, 'dcim')
    cursor = db.cursor()
    cursor.execute(r"SELECT Name from fac_DataCenter WHERE DataCenterID="+str(id))
    result = cursor.fetchone()
    if result:
        return result[0]
    else:
        return 'Unknown DataCenter'

def get_cabinet_location(id):
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, 'dcim')
    cursor = db.cursor()
    cursor.execute(r"select DataCenterID, Location from fac_Cabinet WHERE CabinetID="+str(id))
    result = cursor.fetchone()
    if result:
        return [result[1], get_datacenter_name(result[0])]

    else:
        return 'No Results Fetched from DCIM DB'

def get_assignedto_name(id):
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password, 'dcim')
    cursor = db.cursor()
    cursor.execute(r"SELECT Name from fac_Department WHERE DeptID="+str(id))
    result = cursor.fetchone()
    if result:
        return result[0]
    else:
        return 'General Use'

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

def device_table_printing(field_names, preprint_string, query_string):
    global html_page
    global send_email
    x = HTML_table(field_names, preprint_string)
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password)
    cursor = db.cursor()
    cursor.execute(query_string)
    results = cursor.fetchall()
    if results:
        send_email = True
        for every_result in results:
            every_result = list(every_result)
            index_of_owner = field_names.index('Owner')
            every_result = [get_assignedto_name(value) if x == index_of_owner else value for x,value in enumerate(every_result)]
            index_of_cabinet = field_names.index('Cabinet')
            every_result = [get_cabinet_location(value) if x == index_of_cabinet else value for x, value in
                            enumerate(every_result)]
            x.add_column(list(flatten(every_result)))
        html_page.add_table(x)


def cabinet_table_printing(field_names, preprint_string, query_string):
    global html_page
    global send_email
    x = HTML_table(field_names, preprint_string)
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password)
    cursor = db.cursor()
    cursor.execute(query_string)
    results = cursor.fetchall()
    if results:
        send_email = True
        for every_result in results:
            every_result = list(every_result)
            index_of_owner = field_names.index('AssignedTo')
            every_result = [get_assignedto_name(value) if x == index_of_owner else value for x,value in enumerate(every_result)]
            index_of_datacenterid = field_names.index('DataCenterID')
            every_result = [get_datacenter_name(value) if x == index_of_datacenterid else value for x, value in
                            enumerate(every_result)]
            x.add_column(list(every_result))
        html_page.add_table(x)

def printing_definition(field_names, preprint_string, query_string):
    global html_page
    global send_email
    x = HTML_table(field_names, preprint_string)
    db = pymysql.connect(Config.database_host, Config.db_username, Config.db_password)
    cursor = db.cursor()
    cursor.execute(query_string)
    results = cursor.fetchall()
    if results:
        send_email = True
        if 'AssignedTo' in field_names or 'Owner' in field_names:
            for every_result in results:
                each_result = list(every_result)
                owner_field = each_result.pop()
                owner_field = get_assignedto_name(owner_field)
                each_result.append(owner_field)
                x.add_column(list(each_result))
        else:
            for every_result in results:
                x.add_column(list(every_result))
        html_page.add_table(x)



html_page = HTML_Page()
send_email = False
'''
Kindly Note to add field names Please make sure you put column names correctly. Otherwise the program 
will throw an error saying the column is not found.
'''
###################### DataCenter Field Names ######################
datacenter_fields = ['DataCenterID', 'Name', 'Administrator']
table_name = 'fac_DataCenter'
increment_id = 'DataCenterID'
###################### DataCenter Addition  ########################
query_string = Query_field_generator(Query_Strings.Addition, datacenter_fields, table_name, increment_id)
printing_definition(datacenter_fields, 'DataCenter Additions:', query_string)
###################### DataCenter Deletion  ########################
query_string = Query_field_generator(Query_Strings.Deletion, datacenter_fields, table_name, increment_id)
printing_definition(datacenter_fields, 'DataCenter Deletions:', query_string)

###################### Cabinet Field Names ##########################
cabinet_fields = ['CabinetID', 'DataCenterID', 'Location', 'Model', 'AssignedTo']
table_name = 'fac_Cabinet'
increment_id = 'CabinetID'
###################### Cabinet Addition #############################
query_string = Query_field_generator(Query_Strings.Addition, cabinet_fields, table_name, increment_id)
cabinet_table_printing(cabinet_fields, 'Cabinet Additions:', query_string)
###################### Cabinet Deletion #############################
query_string = Query_field_generator(Query_Strings.Deletion, cabinet_fields, table_name, increment_id)
cabinet_table_printing(cabinet_fields, 'Cabinet Deletions:', query_string)

##################### Device Field Names ############################
device_fields = ['DeviceID', 'Label', 'Owner', 'Cabinet','Position', 'DeviceType']
table_name = 'fac_Device'
increment_id = 'DeviceID'
##################### Device Addition ################################
query_string = Query_field_generator(Query_Strings.Addition, device_fields, table_name, increment_id)
device_table_printing(['DeviceID', 'Label', 'Owner', 'Cabinet', 'DataCenter Name', 'Position', 'DeviceType'],'Device Additions:',query_string)
##################### Device Deletion ################################
query_string = Query_field_generator(Query_Strings.Deletion, device_fields, table_name, increment_id)
device_table_printing(device_fields,'Device Deletions:',query_string)

##########################  Save HTML Page ###############################
html_page.save_table()
if send_email:
    # print('New Changelog Email sent')
    sendemail()



############## Last Operations ##################
# os.system('python delete_contents_from_table.py')
# os.system('python copy_dcim_contents.py')
delete_contents_from_table.delete_all_contents()
copy_dcim_contents.copy_contents()